package controllers;
import play.data.Form;
import play.*;
import play.mvc.*;
import java.io.File;
import play.api.*;
import static play.data.Form.form;
import views.html.*;
import models.*;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Application extends Controller {

    public static class Login{
		public String email;
		public String password;
	}

	public static Result login(){
		return ok(login.render(form(Login.class)));
	}
	
	public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
 
        session().clear();
        if (UserAccount.authenticate(email, password) == null) {
            flash("error", "Email hoặc Mật Khẩu Không Đúng. Đăng Nhập Lại");
            return redirect(routes.Application.login());       
        }
        session("email", email);

		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			session("name", trainee.name);
			return redirect(routes.Trainees.info(trainee));
		}
		
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			session("name", teacher.name);
			return redirect(routes.Teachers.thongtincanhan(teacher));
		}
		return redirect(routes.Trainees.list(0));
    }
	
	public static Result logout(){
		session().clear();
		return redirect(routes.Application.login()); 
	}
	

}

