package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import models.Trainee;
import models.Password;
import play.data.Form;
import views.html.trainees.changePassword;
import views.html.trainees.info_personal;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Passwords extends Controller {
	private static final Form<Password> passwordForm = Form.form(Password.class);
	public static Result changePassword(Trainee trainee){
		return ok(changePassword.render(passwordForm, trainee));
	}
	
	public static Result save(){
		String email=session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
        Form<Password> boundForm = passwordForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(changePassword.render(boundForm, trainee));
		}
		Password password = boundForm.get();
		if(trainee.password.compareTo(password.password_current)!=0){
			flash("error", String.format("Mật khẩu hiện tại không đúng"));
			return ok(changePassword.render(passwordForm, trainee));
		}
		
		if(password.password.compareTo(password.confirm)!=0){
			flash("error", String.format("Không đúng. Nhập lại mật khẩu"));
			return ok(changePassword.render(passwordForm, trainee));

		}
		
		trainee.password=password.password;
		trainee.update();
		return ok(info_personal.render(trainee));
	}
}