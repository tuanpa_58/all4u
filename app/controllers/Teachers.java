package controllers;
import views.html.teachers.list;
import views.html.teachers.details;
import views.html.teachers.catalog;
import views.html.teachers.thongtincanhan;
import views.html.teachers.update;
import views.html.teachers.hoso;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;
import java.util.*;
import play.data.Form;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Teachers extends Controller{
	private static final Form<Teacher> teacherForm =  Form.form(Teacher.class);

	public static Result list(Integer page){
		String email= session().get("email");
		//Chỉ có tài khoản giáo vụ mới có chức năng này
		//Còn các tài khoản khác không cho truy cập
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee != null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher1 = Teacher.findByEmail(email);
		if(teacher1 != null){
			return notFound(String.format("404 File Not Found!"));
		}
		Page<Teacher> teacher = Teacher.find(page);
		return ok(catalog.render(teacher));
	}
	
	public static Result newTeacher(){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		return ok(details.render(teacherForm));
	}

	public static Result details(Teacher teacher){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher1 = Teacher.findByEmail(email);
		if(teacher1!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(teacher==null){
			return notFound(String.format("Teacher %s does not exist.", teacher.email));
		}
		Form<Teacher> filledForm = teacherForm.fill(teacher);
		return ok(details.render(filledForm));
	}
	
	public static Result save(){
		Form<Teacher> boundForm = teacherForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}

		
		Teacher teacher = boundForm.get();

		//Kiểm tra tài khoản email xem có hợp lệ không
		String email_temp = teacher.email;
		String[] tmp = email_temp.split("@");
		if (tmp[1].compareTo("example.com") != 0){
			flash("error","Email không hợp lệ");
			return badRequest(details.render(boundForm));
		}


		if(teacher.id == null){
			if(Teacher.findByEmail(teacher.email)==null){
				teacher.save();
				flash("success", String.format("Thêm mới thành công: %s", teacher));
			}
			else{
				flash("error", String.format("Email %s đã tồn tại !", teacher.email));
			}
		}

		else{
			teacher.update();
			flash("success", String.format("Chỉnh sửa thành công: %s", teacher));

		}
		
		return redirect(routes.Teachers.list(0));

	}

	public static Result delete(String email){
		final Teacher teacher = Teacher.findByEmail(email);
		if(teacher==null){
			return notFound(String.format("teacher %s does not exist.", email));
		}
		
		teacher.delete();
		return redirect(routes.Teachers.list(0));
	}

	public static Result thongtincanhan(Teacher teacher){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(teacher==null){
			return notFound(String.format("Teacher %s does not exist.", teacher.email));
		}
		return ok(thongtincanhan.render(teacher));
	}
	

	public static Result update(Teacher teacher){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(teacher==null){
			return notFound(String.format("Teacher %s does not exist.", teacher.email));
		}
		Form<Teacher> filledForm = teacherForm.fill(teacher);
		return ok(update.render(filledForm, teacher));
	}

	public static Result save_giaovien(){
		Form<Teacher> boundForm = teacherForm.bindFromRequest();
		Teacher teacher = boundForm.get();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(update.render(boundForm, teacher));
		}

		//Kiểm tra tài khoản email xem có hợp lệ không
		String email_temp = teacher.email;
		String[] tmp = email_temp.split("@");
		if (tmp[1].compareTo("example.com") != 0){
			flash("error","Email không hợp lệ");
			return badRequest(details.render(boundForm));
		}

		teacher.update();
		flash("success", String.format("Chỉnh Sửa Thành Công %s", teacher));
		
		return ok(thongtincanhan.render(teacher));

	}
	
	//ho so giao vien
	public static Result hoso(Teacher teacher){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher1 = Teacher.findByEmail(email);
		if(teacher1!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(teacher==null){
			return notFound(String.format("Trainee %s does not exit.", teacher.email));
		}
		
		return ok(hoso.render(teacher));
	
	}
	
}




