package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.trainees.list;
import views.html.trainees.details;
import views.html.trainees.catalog;
import views.html.trainees.personal;
import views.html.trainees.hoso;
import views.html.trainees.info_personal;
import views.html.trainees.update;

import views.html.trainees.changePassword;
import views.html.home;
import views.html.timkiem;
import play.api.data.Forms;
import java.io.File;
import models.Trainee;
import models.Password;
import models.Teacher;
import models.UserAccount;
import java.util.*;
import play.data.Form;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;
@Security.Authenticated(Secured.class)
public class Trainees extends Controller {

	
	private static final Form<Trainee> traineeForm =  Form.form(Trainee.class);
	
	public static Result list(Integer page){
		
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Page<Trainee> trainees = Trainee.find(page);
		return ok(catalog.render(trainees));
	}
	
	public static Result newTrainee(){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		return ok(details.render(traineeForm));
	}
	
	public static Result details(Trainee trainee){
		String email= session().get("email");
		Trainee trainee1 = Trainee.findByEmail(email);
		if(trainee1!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		//final Trainee trainee = Trainee.findByEmail(email);
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exist.", trainee.email));
		}
		Form<Trainee> filledForm = traineeForm.fill(trainee);
		return ok(details.render(filledForm));
	}
	
	public static Result save(){
		Form<Trainee> boundForm = traineeForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}

		
		Trainee trainee = boundForm.get();
		//Kiểm tra tài khoản email xem có hợp lệ không
		String email_temp = trainee.email;
		String[] tmp = email_temp.split("@");
		if (tmp[1].compareTo("example.com") != 0){
			flash("error","Email không hợp lệ");
			return badRequest(details.render(boundForm));
		}
		
		if(trainee.id==null){
			if(Trainee.findByEmail(trainee.email)==null){
				trainee.save();
				flash("success", String.format("Thêm Mới Thành Công : %s", trainee));
			}
		
			else{
				flash("error", String.format("Email %s Đã Tồn Tại !", trainee.email));
			}
		}
		else{
			trainee.update();
			flash("success", String.format("Chỉnh Sửa Thành Công : %s", trainee));
		}
		
		return redirect(routes.Trainees.list(0));
	}

	public static Result delete(String email){
		final Trainee trainee = Trainee.findByEmail(email);
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exist.", email));
		}
		
		trainee.delete();
		flash("success", String.format("Xóa Thành Công %s", email));
		return redirect(routes.Trainees.list(0));
	}
	
	public static Result personal(Trainee trainee){
		//final Trainee trainee = Trainee.findByEmail(email);
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exist.", trainee.email));
		}
		//Form<Trainee> filledForm = traineeForm.fill(trainee);
		return ok(personal.render(trainee));
	}
	
	public static Result hoso(Trainee trainee){
		String email= session().get("email");
		Trainee trainee1 = Trainee.findByEmail(email);
		if(trainee1!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exit.", trainee.email));
		}
		
		return ok(hoso.render(trainee));
	
	}
	
	//thong tin ca nhan
	public static Result info(Trainee trainee){
		String email= session().get("email");
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exit.", trainee.email));
		}
		
		return ok(info_personal.render(trainee));
	}
	
	//ham sua thong tin

	public static Result update(Trainee trainee){
		String email= session().get("email");
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		if(trainee==null){
			return notFound(String.format("Trainee %s does not exist.", trainee.email));
		}
		Form<Trainee> filledForm = traineeForm.fill(trainee);
		return ok(update.render(filledForm, trainee));
	}

	//ham luu thong tin hoc vien chinh sua
	public static Result save_hocvien(){
		Form<Trainee> boundForm = traineeForm.bindFromRequest();
		Trainee trainee = boundForm.get();

		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(update.render(boundForm, trainee));
		}

		//Kiểm tra tài khoản email xem có hợp lệ không
		String email_temp = trainee.email;
		String[] tmp = email_temp.split("@");
		if (tmp[1].compareTo("example.com") != 0){
			flash("error","Email không hợp lệ");
			return badRequest(update.render(boundForm,trainee));
		}
		
		trainee.update();
		flash("success", String.format("Chỉnh Sửa Thành Công %s", trainee));
		
		return ok(info_personal.render(trainee));

	}

	public static Result home(){
		String email= session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
		if(trainee!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		return ok(home.render());
	}
	
	private static final Form<Password> passwordForm = Form.form(Password.class);
	public static Result changePassword(Trainee trainee){
		String email= session().get("email");
		Teacher teacher = Teacher.findByEmail(email);
		if(teacher!=null){
			return notFound(String.format("404 File Not Found!"));
		}
		return ok(changePassword.render(passwordForm, trainee));
	}
	
	public static Result savePassword(){
		String email=session().get("email");
		Trainee trainee = Trainee.findByEmail(email);
        Form<Password> boundForm = passwordForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(changePassword.render(boundForm, trainee));
		}
		Password password = boundForm.get();
		if(trainee.password.compareTo(password.password_current)!=0){
			flash("error_passcurrent", String.format("Mật Khẩu Hiện Tại Không Đúng"));
			return ok(changePassword.render(passwordForm, trainee));
		}
		
		if(password.password.compareTo(password.confirm)!=0){
			flash("error", String.format("Nhập Lại Mật Khẩu Không Đúng"));
			return ok(changePassword.render(passwordForm, trainee));

		}
		
		trainee.password=password.password;
		trainee.update();
		flash("success", String.format("Đổi Mật Khẩu Thành Công"));
		return redirect(routes.Trainees.info(trainee));
	}
	

	
}