package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.data.Form;
import models.Trainee;
import views.html.userpage.*;
import play.mvc.Security;
import com.avaje.ebean.Page;

@Security.Authenticated(Secured.class)
public class UserPage extends Controller {
    
    private static final Form<Trainee> traineeForm = Form.form(Trainee.class);
    
	public static Result portal(String email) {
        if(Trainee.findByEmail(session().get("email")) != null) {
            Trainee trainee = Trainee.findByEmail(email);
            return ok(portal.render(trainee));
        } else {
            Page<Trainee> trainees = Trainee.find(0);
            flash("error", "You do not have sufficient permissions to access that page!");
            return ok(views.html.catalog.render(trainees));
        }
    }
    
    public static Result profile(String email) {
        if(Trainee.findByEmail(session().get("email")) != null) {
            final Trainee trainee = Trainee.findByEmail(email);
            if(trainee == null) {
                return notFound(String.format("%s does not exist.", trainee));
            }
            return ok(profile.render(trainee));
        } else {
            Page<Trainee> trainees = Trainee.find(0);
            flash("error", "You do not have sufficient permissions to access that page!");
            return ok(views.html.catalog.render(trainees));
        }
    }
    
    public static Result update(String email) {
        if(Trainee.findByEmail(session().get("email")) != null) {
            final Trainee trainee = Trainee.findByEmail(email);
            if(trainee == null) {
                return notFound(String.format("%s does not exist.", trainee));
            }
            Form<Trainee> filledForm = traineeForm.fill(trainee);
            return ok(update.render(filledForm));
        } else {
            Page<Trainee> trainees = Trainee.find(0);
            flash("error", "You do not have sufficient permissions to access that page!");
            return ok(views.html.catalog.render(trainees));
        }
    }
    
    public static Result save() {
        Form<Trainee> boundForm = traineeForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(update.render(boundForm));
        }
        Trainee trainee = boundForm.get();
        trainee.update();
        flash("success", String.format("Successfully updated %s", trainee));
        return redirect(routes.UserPage.portal(trainee.email));
    }
}
