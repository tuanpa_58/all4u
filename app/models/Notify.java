package models;
import java.util.ArrayList;
import java.util.List;
import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.mvc.PathBindable;
import java.util.*;

@Entity
public class Notify extends Model {
	@Id 
	public Long id;
	public String notify;

	public Date date;
	
	public Notify(){}
	public static Finder<Long, Notify> find = new Finder<Long, Notify>(Long.class , Notify.class);
	public static List<Notify> findAll(){
		return find.all();
	}
}