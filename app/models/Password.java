package models;
import java.util.ArrayList;
import java.util.List;
import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.mvc.PathBindable;

import java.io.*;
@Entity
public class Password extends Model {
	@Id
    public Long id;

	@Constraints.Required
	public String password_current;
	
	@Constraints.Required
	public String password;
	
	@Constraints.Required
	public String confirm;
	
	public Password(){}
	public Password(String password_current, String password, String confirm){
		this.password_current = password_current;
		this.password = password;
		this.confirm = confirm;
	}

}