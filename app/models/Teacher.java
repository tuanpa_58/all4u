package models;
import java.util.ArrayList;
import java.util.List;
import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.mvc.PathBindable;

@Entity
public class Teacher extends Model implements PathBindable<Teacher>{
	@Constraints.Required
	public String name;
	
	@Constraints.Required
	public String jobTitle;
	
	public String phoneNumber;
	
	@Constraints.Required	
	public String office;
	
	@Constraints.Required	
	public String email;
	
	@Constraints.Required
	public String password;
	
	@Id
	public Long id=null;
	
	public Teacher(){}
	public Teacher(String name, String jobTitle, String office, String phoneNumber
					, String email, String password){
			this.name=name;
			this.jobTitle=jobTitle;
			this.phoneNumber=phoneNumber;
			this.email=email;
			this.password=password;
			this.office=office;
	}
	
	public String toString(){
		return String.format("%s", name);
	}
	
	private static List<Teacher> teachers;

	public static Finder<Long , Teacher> find = new Finder<Long , Teacher>(Long.class, Teacher.class);
	
	public static List<Teacher> findAll(){
		return find.all();
	}
	
	//ham dinh nghia phan trang
	public static Page<Teacher> find(int page){
		return find.where()
			.orderBy("id asc")     //sap xep tang dan theo id
			.findPagingList(5)    // quy định kích thước của trang
            .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
            .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
	}
	
	public static Teacher findByEmail(String email){
		return find.where().eq("email", email).findUnique();
	}
	
	public static List<Teacher> findByName(String name){
		final List<Teacher> results= new ArrayList<Teacher>();
		for(Teacher candidate : teachers){
			if(candidate.name.toLowerCase().contains(name.toLowerCase())){
				results.add(candidate);
			}
		}
		return results;
	}
	

	
	@Override
    public Teacher bind(String key, String value) {
        return findByEmail(value);
    }
 
    @Override
    public String unbind(String key) {
        return email;
    }
 
    @Override
    public String javascriptUnbind() {
        return email;
    }

	
}
