package models;
import java.util.ArrayList;
import java.util.List;
import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.mvc.PathBindable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.*;

@Entity
public class Trainee extends Model implements PathBindable<Trainee>{
	public SimpleDateFormat format= new SimpleDateFormat("dd-MM-yyyy");

	@Constraints.Required
	public String name;
	@Constraints.Required
	public Date birthday;
	public String address;
	public String phoneNumber;
	@Constraints.Required
	public String branch;
	public String course;
	@Constraints.Required
	public String email;
	@Constraints.Required
	public String password;
	
	
	@Id
	public Long id=null;
	
	public Trainee(){}
	public Trainee(String name, Date birthday, String address, String phoneNumber
					, String branch, String course, String email, String password){
			this.name=name;
			this.birthday=birthday;
			this.address=address;
			this.phoneNumber=phoneNumber;
			this.branch=branch;
			this.course=course;
			this.email=email;
			this.password=password;
	}
	
	public String toString(){
		return String.format("%s", name);
	}
	
	private static List<Trainee> trainees;
	static{
		trainees = new ArrayList<Trainee>();
		trainees.add(new Trainee("Phan Anh Tuan", new Date("12/6/1995"), "Nam Dinh",
					"01657299953", "CNTT", "2013-2017", "tuanpa_58@vnu.edu.vn", "123456789"));
	}
	
	public static Finder<Long , Trainee> find = new Finder<Long , Trainee>(Long.class, Trainee.class);
	
	public static List<Trainee> findAll(){
		return find.all();
	}
	
	//ham dinh nghia phan trang
	public static Page<Trainee> find(int page){
		return find.where()
			.orderBy("id asc")     //sap xep tang dan theo id
			.findPagingList(5)    // quy định kích thước của trang
            .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
            .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
	}
	
	public static Trainee findByEmail(String email){
		return find.where().eq("email", email).findUnique();
	}
	
	public static List<Trainee> findByName(String name){
		final List<Trainee> results= new ArrayList<Trainee>();
		for(Trainee candidate : trainees){
			if(candidate.name.toLowerCase().contains(name.toLowerCase())){
				results.add(candidate);
			}
		}
		return results;
	}
	
	public static boolean remove(Trainee trainee){
		return trainees.remove(trainee);
	}

	@Override
    public Trainee bind(String key, String value) {
        return findByEmail(value);
    }
 
    @Override
    public String unbind(String key) {
        return email;
    }
 
    @Override
    public String javascriptUnbind() {
        return email;
    }

	
}






