package models;
 
import play.data.validation.Constraints;
import play.db.ebean.Model;
import controllers.*;
import javax.persistence.Entity;
import javax.persistence.Id;
 import play.data.Form;
@Entity
public class UserAccount extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
 
    public UserAccount() { }
    public UserAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }
 
    public static UserAccount authenticate(String email, String password) {
		Trainee trainee = Trainee.findByEmail(email);
		Teacher teacher = Teacher.findByEmail(email);
		if(trainee!=null){
			if(trainee.password.compareTo(password)==0){
				
				return new UserAccount(trainee.email, trainee.password);
			}		
			return null;
		}
		
		if(teacher!=null){
			if(teacher.password.compareTo(password)==0)
				return new UserAccount(teacher.email, teacher.password);
			return null;
		}
		
        return finder.where().eq("email", email).eq("password", password).findUnique();

    }
 
	
    public static Finder<Long, UserAccount> finder = new Finder<Long, UserAccount>(Long.class, UserAccount.class);
}