name := "HocVien"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  "com.google.guava" % "guava" % "14.0",
  filters
)

libraryDependencies += javaEbean

libraryDependencies += filters
