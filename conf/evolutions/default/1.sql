# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table notify (
  id                        bigint not null,
  notify                    varchar(255),
  date                      timestamp,
  constraint pk_notify primary key (id))
;

create table password (
  id                        bigint not null,
  password_current          varchar(255),
  password                  varchar(255),
  confirm                   varchar(255),
  constraint pk_password primary key (id))
;

create table teacher (
  id                        bigint not null,
  name                      varchar(255),
  job_title                 varchar(255),
  phone_number              varchar(255),
  office                    varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_teacher primary key (id))
;

create table trainee (
  id                        bigint not null,
  name                      varchar(255),
  birthday                  timestamp,
  address                   varchar(255),
  phone_number              varchar(255),
  branch                    varchar(255),
  course                    varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_trainee primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence notify_seq;

create sequence password_seq;

create sequence teacher_seq;

create sequence trainee_seq;

create sequence user_account_seq;




# --- !Downs

drop table if exists notify cascade;

drop table if exists password cascade;

drop table if exists teacher cascade;

drop table if exists trainee cascade;

drop table if exists user_account cascade;

drop sequence if exists notify_seq;

drop sequence if exists password_seq;

drop sequence if exists teacher_seq;

drop sequence if exists trainee_seq;

drop sequence if exists user_account_seq;

